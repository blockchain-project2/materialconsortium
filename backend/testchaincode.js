async function main() {
    // Load the network configuration
    const ccpPath = path.resolve(__dirname, '.', 'connection.json');
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

    // Create a new file system-based wallet for managing identities.
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);

    // Check to see if we've already enrolled the user.
    const identity = await wallet.get('Admin@cottonproducers.com');
    if (!identity) {
        console.log(`An identity for the user "Admin@cottonproducers.com" does not exist in the wallet`);
        return;
    }

    // Create a new gateway instance for interacting with the fabric network.
    const gateway = new Gateway();
    try {
        // Connect to the gateway using the identity from wallet and the connection profile.
        await gateway.connect(ccp, {
            wallet, identity, discovery: {
                enabled: true, asLocalhost: false  // Set asLocalhost according to your environment
            }
        });

        // Get the network (channel) and the contract
        const network = await gateway.getNetwork('textilechannel');  // Use correct channel name
        const contract = network.getContract('textilemgt');

        // Invoke the chaincode with necessary arguments

        // Create a new textile
        console.log('\n--> Peer Chaincode Invoke: CreateTextile');
        await contract.submitTransaction('CreateTextile', '1', 'Cotton Fabric', 'Cotton', '2023-01-01T00:00:00Z', '100');

        // Query the created textile
        console.log('\n--> Peer Chaincode Query: ReadTextile');
        let result = await contract.evaluateTransaction('ReadTextile', '1');
        console.log(`Result: ${prettyJSONString(result.toString())}`);

        // Update the textile
        console.log('\n--> Peer Chaincode Invoke: UpdateTextile');
        await contract.submitTransaction('UpdateTextile', '1', 'Silk Fabric', 'Silk', '2023-05-01T00:00:00Z', '200');

        // Delete the textile
        console.log('\n--> Peer Chaincode Invoke: DeleteTextile');
        await contract.submitTransaction('DeleteTextile', '1');

    } finally {
        // Disconnect from the gateway when done
        gateway.disconnect();
    }
}
