/****************************
 * JS Script for index.html
 ****************************/

// Function to clear the Textile Information card
function clearCard() {
    $('#editTextileId').text("");
    $('#editTextileName').val("");
    $('#editTextileType').val("");
    $('#editManufactureDate').val("");
    $('#editQuantity').val("");
    $('#message').text("");
    $('#editTextileForm')[0].reset();
    $('#editTextileForm').addClass('d-none');
}

// Function to format the date
function formattedDate(rfc3339Date) {
    var date = new Date(rfc3339Date);
    var day = date.getDate(); // day of the month
    var month = date.getMonth() + 1; // month (getMonth() returns 0-11, so add 1)
    month = month < 10 ? "0" + month : month;
    day = day < 10 ? "0" + day : day;
    var year = date.getFullYear(); // year
    // format date as YYYY-MM-DD
    var formattedDate = year + '-' + month + '-' + day;
    return formattedDate;
}

// Function to call when 'No' is clicked
function noFunction() {
    $('#confirmModal').modal('hide');
}

// Add Textile record to ledger
function createTextileRecord() {
    // Get form data
    var textileId = $('#textileId').val();
    var textileName = $('#textileName').val();
    var manufactureDate = $('#manufactureDate').val();
    var textileType = $('#textileType').val();
    var quantity = $('#quantity').val();

    var data = {
        id: textileId,
        name: textileName,
        manufactureDate: manufactureDate,
        type: textileType,
        quantity: quantity
    };

    // Send a POST request to the server
    $.ajax({
        url: '/textiles', // replace with your URL
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {
            // Show the success modal
            var myModal = new bootstrap.Modal($('#successModal'), {});
            myModal.show();
        },
        error: function (error) {
            console.log(error);
        }
    }).always(function () {
        // Clear the form
        $('#createTextileForm')[0].reset();
        $('#confirmModal').modal('hide');
        $('#yesButton').off("click");
    });
}

// Remove a Textile record from ledger
function deleteTextileRecord() {
    var textileId = $('#editTextileId').text(); // Get the value from the element
    $.ajax({
        url: '/textiles/' + textileId, // append the textileId to your API endpoint
        type: 'DELETE',
        success: function (result) {
            // Show the success modal
            var myModal = new bootstrap.Modal($('#successModal'), {});
            myModal.show();
            console.log(result);
        },
        error: function (error) {
            // Handle error
            console.log(error);
        }
    }).always(function () {
        // Clear the card
        clearCard();
        $('#confirmModal').modal('hide');
        $('#yesButton').off("click");
    });
}

// Edit a Textile record in the ledger
function updateTextileRecord() {
    var textileName = $('#editTextileName').val();
    var manufactureDate = $('#editManufactureDate').val();
    var textileType = $('#editTextileType').val();
    var quantity = $('#editQuantity').val();
    
    var data = {
        name: textileName,
        manufactureDate: manufactureDate,
        type: textileType,
        quantity: quantity
    };

    var textileId = $('#editTextileId').text();
    $.ajax({
        url: '/textiles/' + textileId, // append the textileId to your API endpoint
        type: 'PUT',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {
            var myModal = new bootstrap.Modal($('#successModal'), {});
            myModal.show();
            clearCard()
            console.log(result);
        },
        error: function (error) {
            // Handle error
            console.log(error);
        }
    }).always(function () {
        // Clear the card
        clearCard();
        $('#confirmModal').modal('hide');
        $('#yesButton').off("click");
    });
}

$(document).ready(function () {
    // Add event Handlers

    // Event listener for when the modal is hidden
    $('#confirmModal').on('hidden.bs.modal', function () {
        console.log(result); // Outputs: true if 'Yes' was clicked, false if 'No' was clicked
    });

    // Attach event listeners to the buttons
    $('#noButton').click(noFunction);

    // Handle form submission for creating textile record
    $('#createTextileForm').on('submit', function (e) {
        e.preventDefault();
        $('#yesButton').off("click");
        $('#yesButton').click(createTextileRecord);
        $('#confirmModal').modal('show');
    });

    // Handle form submission for retrieving textile record
    $('#getTextileForm').on('submit', function (e) {
        e.preventDefault();
        var searchId = $('#searchId').val();
        $.ajax({
            url: '/textiles/' + searchId,
            type: 'GET',
            success: function (data) {
                $('#editTextileId').text(data.id);
                $('#editTextileName').val(data.name);
                $('#editManufactureDate').val(formattedDate(data.manufactureDate));
                $('#editTextileType').val(data.type);
                $('#editQuantity').val(data.quantity);
                $('#editTextileForm').removeClass('d-none');
            },
            error: function (error) {
                clearCard()
                $('#message').text("Not Found");
                console.log(error);
            }
        });
    });

    // Handle delete button click
    $('#deleteButton').click(function () {
        $('#yesButton').off("click");
        $('#yesButton').click(deleteTextileRecord);
        $('#confirmModal').modal('show');
    });

    // Handle update button click
    $('#updateButton').click(function () {
        $('#yesButton').off("click");
        $('#yesButton').click(updateTextileRecord);
        $('#confirmModal').modal('show');
    });
});
