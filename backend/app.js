const express = require('express');
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

const app = express();
app.use(express.json());

// Static Middleware
app.use(express.static(path.join(__dirname, 'public')));

app.post('/textiles', async (req, res) => {
    try {
        const { id, name, type, manufacture, quantity } = req.body;
        const result = await submitTransaction('CreateTextile', id, name, type, new Date(manufacture), quantity);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

app.get('/textiles/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const result = await evaluateTransaction('ReadTextile', id);
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(404).send(`Failed to evaluate transaction: ${error}`);
    }
});

app.put('/textiles/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const { name, type, manufacture, quantity } = req.body;
        const result = await submitTransaction('UpdateTextile', id, name, type, new Date(manufacture), quantity);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

app.delete('/textiles/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const result = await submitTransaction('DeleteTextile', id);
        res.send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

async function getContract() {
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const identity = await wallet.get('Admin@cottonproducers.com');
    const gateway = new Gateway();
    const connectionProfile = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'connection.json'), 'utf8'));
    const connectionOptions = { wallet, identity: identity, discovery: { enabled: false, asLocalhost: true } };
    await gateway.connect(connectionProfile, connectionOptions);
    const network = await gateway.getNetwork('textilechannel');
    const contract = network.getContract('textilemgt');
    return contract;
}

async function submitTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.submitTransaction(functionName, ...args);
    return result.toString();
}

async function evaluateTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.evaluateTransaction(functionName, ...args);
    return result.toString();
}

app.get('/', (req, res) => {
    res.send('Hello, World!');
});

module.exports = app;
