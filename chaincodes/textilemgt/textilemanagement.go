package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type Textile struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	Type        string    `json:"type"`
	Manufacture time.Time `json:"manufacture"`
	Quantity    int       `json:"quantity"`
}

// RecordCount struct keeps track of the number of records
type RecordCount struct {
	Count int `json:"count"`
}

type TextileContract struct {
	contractapi.Contract
}

func (t *TextileContract) CreateTextile(ctx contractapi.TransactionContextInterface, id string, name string,
	textileType string, manufacture time.Time, quantity int) error {
	// Check if a textile record with the given id already exists
	existingTextileJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return err
	}
	if existingTextileJSON != nil {
		return fmt.Errorf("a textile record with the id '%s' already exists", id)
	}

	textile := Textile{
		ID:          id,
		Name:        name,
		Type:        textileType,
		Manufacture: manufacture,
		Quantity:    quantity,
	}
	textileJSON, err := json.Marshal(textile)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(id, textileJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	eventPayload := fmt.Sprintf("Created textile: %s", id)
	err = ctx.GetStub().SetEvent("CreateTextile", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	// Update record count
	return t.UpdateRecordCount(ctx, 1)
}

func (t *TextileContract) ReadTextile(ctx contractapi.TransactionContextInterface, id string) (*Textile, error) {
	textileJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if textileJSON == nil {
		return nil, fmt.Errorf("the textile %s does not exist", id)
	}
	var textile Textile
	err = json.Unmarshal(textileJSON, &textile)
	if err != nil {
		return nil, err
	}
	eventPayload := fmt.Sprintf("Read textile: %s", id)
	err = ctx.GetStub().SetEvent("ReadTextile", []byte(eventPayload))
	if err != nil {
		return nil, fmt.Errorf("event failed to register. %v", err)
	}
	return &textile, nil
}

func (t *TextileContract) UpdateTextile(ctx contractapi.TransactionContextInterface, id string, name string,
	textileType string, manufacture time.Time, quantity int) error {
	textile, err := t.ReadTextile(ctx, id)
	if err != nil {
		return err
	}
	textile.Name = name
	textile.Type = textileType
	textile.Manufacture = manufacture
	textile.Quantity = quantity
	textileJSON, err := json.Marshal(textile)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(id, textileJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	eventPayload := fmt.Sprintf("Updated textile: %s", id)
	err = ctx.GetStub().SetEvent("UpdateTextile", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	return nil
}

func (t *TextileContract) DeleteTextile(ctx contractapi.TransactionContextInterface, id string) error {
	err := ctx.GetStub().DelState(id)
	if err != nil {
		return fmt.Errorf("failed to delete state: %v", err)
	}
	eventPayload := fmt.Sprintf("Deleted textile: %s", id)
	err = ctx.GetStub().SetEvent("DeleteTextile", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	return nil
}

// UpdateRecordCount updates the total number of textile records
func (t *TextileContract) UpdateRecordCount(ctx contractapi.TransactionContextInterface, increment int) error {
	countJSON, err := ctx.GetStub().GetState("recordCount")
	if err != nil {
		return err
	}
	var recordCount RecordCount
	if countJSON == nil {
		recordCount = RecordCount{Count: 0}
	} else {
		err = json.Unmarshal(countJSON, &recordCount)
		if err != nil {
			return err
		}
	}
	recordCount.Count += increment
	newCountJSON, err := json.Marshal(recordCount)
	if err != nil {
		return err
	}
	return ctx.GetStub().PutState("recordCount", newCountJSON)
}

// GetRecordCount returns the total number of textile records
func (t *TextileContract) GetRecordCount(ctx contractapi.TransactionContextInterface) (int, error) {
	countJSON, err := ctx.GetStub().GetState("recordCount")
	if err != nil {
		return 0, err
	}
	var recordCount RecordCount
	err = json.Unmarshal(countJSON, &recordCount)
	if err != nil {
		return 0, err
	}
	return recordCount.Count, nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(TextileContract))
	if err != nil {
		fmt.Printf("Error creating textile chaincode: %s", err.Error())
		return
	}
	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting textile chaincode: %s", err.Error())
	}
}
